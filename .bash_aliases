shopt -s expand_aliases  
# Invert colors
alias dark="xcalib -i -a"

# Use aliases to fix missing space typos
alias cd..="cd .."
alias ..="cd .."

alias upgrade="sudo apt-get update && sudo apt-get upgrade"

alias update="sudo apt-get update"

alias get="sudo apt-get upgrade"

# Count the files in the current directory
alias count="ls -1 | wc -l"

# Reverse the first and the second argument of the last command. It would be nice if we could swap the last and the penultimate argument. But I don't know how to get the penultmiate one. This didn't work: https://stackoverflow.com/questions/11054939/how-to-get-the-second-last-argument-from-shell-script
alias swapArgs='$(history -p !:0 !:2 !:1)'

# Vars for paths
marakaiHome="/media/kuranda/windows/Users/marakai"
documentsDir="$marakaiHome/Documents"
wikiDir="/var/www/wiki"
configsDir="~/dotfiles"
vimConfDir="$marakaiHome/.vim"
debianConfDir="$configsDir/debian_conf"
studyTrackDir="~/Studies/study_track"
devDir="~/dev"
listsNotesCheetSheats="$documentsDir/lists,\ notes,\ cheat\ sheets"
bullBytesDir="$documentsDir/job/Bull\ Bytes\ 2016"

# Aliases that take you places
alias configs="cd $configsDir"
alias vimconf="cd $vimConfDir"
alias debianconf="cd $debianConfDir"
alias docs="cd $documentsDir"
alias studytrack="cd $studyTrackDir"
alias dev="cd $devDir"
alias fm="cd $marakaiHome/fm_workspace/FM_Launcher"
alias marakai="cd $marakaiHome/"

# Aliases that open files
alias aliases="vim $debianConfDir/.bash_aliases"
alias journal="vim $marakaiHome/Documents/journal_2016.txt"
alias vimrc="vim $marakaiHome/.vimrc"
alias bashrc="vim $debianConfDir/.bashrc"
alias vocab="vim $listsNotesCheetSheats/vocab_list.txt"
alias notes="vim $listsNotesCheetSheats/general_notes.txt"
alias shopping="vim $listsNotesCheetSheats/shopping.txt"
alias wikiLocalSettings="sudo vim $wikiDir/LocalSettings.php"
alias keydb="keepassx $documentsDir/no_upload/key_db.kdbx &"
alias zeitaufzeichnung_gmr="libreoffice $documentsDir/job/GMR/GMR_Zeitaufzeichnung_Matthias_Linux.xlsx &"
alias bank="file-roller $documentsDir/no_upload/bank.7z &"
alias visa="vim $documentsDir/no_upload/visa.txt"
alias firma="vim $bullBytesDir/firmendaten.txt"

# Aliases that start applications
alias intellij="/usr/local/lib/idea-IU-143.1821.5/bin/idea.sh &"

# Create a bookmark to the user's current or a specified dir by creating an alias that changes to that directory
bookmark(){
  if (( $# == 1 ));then
    bookmarkDir=$PWD
    
  elif (( $# == 2 ));then
    bookmarkDir=$2

  else 
    echo "Usage: First param -> Name of the bookmark. Second Param -> Dir of the bookmark (optional, if not given, use current dir)"
    return -1
  fi

  bookmarkName=$1
  # Absolute path to this script
  scriptPath=$(cd `dirname "${BASH_SOURCE[0]}"` && pwd)/`basename "${BASH_SOURCE[0]}"`
  # Append the bookmark as an alias to this script
  echo "alias $bookmarkName=\"cd '$bookmarkDir'\"" >> $scriptPath
  # Make the changes effective
  source $scriptPath
  echo "new bookmark: $bookmarkName --> $bookmarkDir"
}

# Custom 'find' that searches case-insensitively from root and doesn't display errors
rfind(){
  if (( $# == 1 ));then
    searchExpr=$1
    find / -iname $searchExpr 2>/dev/null
  else 
    echo "Usage: 'rfind searchExpr' searches for searchExpr starting at root, case-insensitive. Won't display errors"
    return -1
  fi
}

# Aliases added by the bookmark function
alias scalajs="cd '/home/kuranda/dev/workspace/scala/scala_js'"
alias forticlient="cd '/media/kuranda/windows/Users/marakai/GMR-plan/VPN/FortiClient 4.0 MR2 Patch8 (Windows 8)/FortiClientVPN_4.2.8.0307'"
alias cvs="cd '/home/kuranda/dev/workspace/scala/cvs'"
alias vim_conf="cd '/home/kuranda/dotfiles/vim_conf'"
alias elm="cd '/home/kuranda/dev/elm/projects'"
