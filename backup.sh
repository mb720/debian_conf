# Cron calls this script: cd into the directory whose contents are backed up
thisDir="/home/kuranda/dotfiles/debian_conf"
cd $thisDir

currDate=`date +%Y-%m-%d_%H:%M:%S`

# Push all files in the folder to the remote repository
git add -A
git commit -m "Debian config files were automatically backed up in $thisDir on $currDate"
git push

